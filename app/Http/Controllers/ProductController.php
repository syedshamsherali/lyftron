<?php

namespace App\Http\Controllers;

use App\Exports\ExcelExport;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    public function index()
    {
        return view('product.index');
    }

    public function create()
    {
        return view('product.create');
    }

    public function store(Request $request)
    {
       $status = Product::create($request->all());
       if($status){
           session()->put('success','Product Save Successfully');
       }else{
           session()->put('error','Product Not Save');
       }
       return redirect()->back();
    }

    public function show()
    {
        $data = Product::orderBy('id','desc')->get();
        return view('product.all-products',compact('data'));
    }

    public function edit(Request $request,$id)
    {
        $product = Product::find($id);
        if(!$product){
            session()->put('error','Product Not Found');
            return redirect()->back();
        }

        return view('product.edit',compact('product'));
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if(!$product){
            session()->put('error','Product Not Found');
            return redirect()->back();
        }

        $product->update($request->all());

        session()->put('success','Product Updated Successfully');
        return redirect()->route('show');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if(!$product){
            session()->put('error','Product Not Found');
            return redirect()->back();
        }

        $product->delete();

        session()->put('success','Product Deleted Successfully');
        return redirect()->back();
    }

    public function csv_export(){
        return Excel::download(new ExcelExport, 'excel.csv');
    }
}
