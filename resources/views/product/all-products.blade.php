@extends('layout.app')
@section('title','All Products')
@section('content')
    <div class="container mt-5">

        <div class="row mt-3">
            <div class="col-md-6">
                <a href="{{route('index')}}" class="btn btn-dark">Back</a>
            </div>
            <div class="col-md-6">
                <a href="{{route('csv-export')}}" class="btn btn-success float-right">CSV Export</a>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-12">
                <table class="table table-striped text-center">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($data) > 0)
                            @foreach($data as $each )
                            <tr>
                                <td>{{$each->title}}</td>
                                <td>{{$each->price}}</td>
                                <td>{{$each->description}}</td>
                                <td>
                                    <a href="{{route('edit',$each->id)}}" class="btn btn-success">Edit</a>
                                    <a href="{{route('delete',$each->id)}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
