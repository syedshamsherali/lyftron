@extends('layout.app')
@section('title','Home')
@section('content')

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6">
                <a href="{{route('create')}}" class="btn btn-success float-right">Add Product</a>
            </div>
            <div class="col-md-6">
                <a href="{{route('show')}}" class="btn btn-success">All Products</a>
            </div>
        </div>
    </div>
@endsection
