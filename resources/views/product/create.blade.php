@extends('layout.app')
@section('title','Create')
@section('style')
    <style>
        label {
            float: right;
        }
    </style>
@endsection
@section('content')
    <div class="container text-center mt-5">

        <div class="row mt-3">
            <div class="col-md-6">
                <a href="{{route('index')}}" class="btn btn-dark">Back</a>
            </div>
        </div>

        <form action="{{route('store')}}" method="post">
            @csrf
            <div class="row mt-3">
                <div class="col-md-6">
                    <label for="title">Title *</label>
                </div>
                <div class="col-md-6">
                    <input type="text" id="title" class="form-control" name="title" placeholder="Title" required>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-6">
                    <label for="price">Price *</label>
                </div>
                <div class="col-md-6">
                    <input type="number" id="price" class="form-control" min="1" name="price" placeholder="Price" required>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-6">
                    <label for="description">Description *</label>
                </div>
                <div class="col-md-6">
                    <textarea id="description" class="form-control" name="description" required rows="5" placeholder="Description"></textarea>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success float-right">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
